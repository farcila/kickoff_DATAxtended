# DigiTaL TP7 DATAxtended - Example Repository for the Project KickOff 02-03-2022

This repository contains two JupyterNotebooks developed for the DATAx W/S 2021 course: "Introduction to programming with Python for data analysis".

It is part of the Demonstration made on the [DigiTaL](https://www.leuphana.de/universitaet/lehre/projekte/digital-transformation-lab-for-teaching-and-learning/digital-teilprojekte.html) Project KickOff and is intended to illustrate a Student's development during the course. It also provides an insight on the potential work with the [GWDG JupyterHub](https://jupyter-cloud.gwdg.de/) solution for future Iterations of the Project.

These Notebooks aren't meant to be used outside the context of this exercise. 

A short manual to get hands-on on this demo is available [here](https://pad.gwdg.de/s/_SVZfQnWG).


 
